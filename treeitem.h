#ifndef TREEITEM_H
#define TREEITEM_H

#include <QtCore>
#include <QList>
#include <QVariant>

class TreeItem
{
public:
    TreeItem(const QList<QVariant> &data, TreeItem *parent = 0);
    TreeItem(const QString &data, TreeItem *parent = 0);
    ~TreeItem();

    void appendChild(TreeItem *child);

    TreeItem *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    TreeItem *parent();
    Qt::CheckState checkState() const { return state; }
    bool isChecked() const { return (state == Qt::Checked) ? true : false;}
    void setCheckState(Qt::CheckState state) { this->state = state; }

private:
    QList<TreeItem*> childItems;
    QList<QVariant> itemData;
    TreeItem *parentItem;
    Qt::CheckState state;
};

#endif // TREEITEM_H
