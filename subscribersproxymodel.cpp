#include "subscribersproxymodel.h"
#include <QtCore>

SubscribersProxyModel::SubscribersProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{

}


void SubscribersProxyModel::setSourceModel(QAbstractItemModel *model)
{
    QSortFilterProxyModel::setSourceModel(model);
    setSourceRootsCounts();

    reset();
}


void SubscribersProxyModel::setFilterList(const QString listName)
{
    this->listName = listName;
    invalidateFilter();
}


bool SubscribersProxyModel::filterAcceptsRow(
        int sourceRow,
        const QModelIndex &sourceParent) const
{
    if (!sourceModel())
        return false;

    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    QModelIndex index1 = sourceModel()->index(sourceRow, 1, sourceParent);

    bool result = sourceModel()->data(index0).toString().contains(listName) ||
            sourceModel()->data(index1.parent()).toString().contains(listName);

    //    qDebug() << "sourceRow:" << sourceRow
    //             << "sourceParent" << sourceParent
    //             << result;

    return result;
}


QModelIndex SubscribersProxyModel::mapFromSource(const QModelIndex &source) const
{
    QModelIndex proxy;

    if (!sourceModel())
        proxy = QModelIndex();

    if (!source.parent().isValid())
        proxy = index(source.row(), 0);
    else {
        int row = rowFromSource(source.parent().row(), source.row());
        proxy = index(row, source.column());
    }

    //    qDebug() << source << "->" << proxy;

    return proxy;
}


QModelIndex SubscribersProxyModel::mapToSource(const QModelIndex &proxy) const
{
    if (!sourceModel())
        return QModelIndex();

    if (!proxy.isValid())
        return QModelIndex();

    QPair<int, int> pos = rowToSource(proxy.row());
    int rootRow = pos.first;
    int row = pos.second;

    QModelIndex parentSource = sourceModel()->index(rootRow, proxy.column());

    if (proxy.column() == 0)
        return parentSource;

    QModelIndex sourceIndex = sourceModel()->index(
                row, proxy.column(), parentSource);

    return sourceIndex;
}


int SubscribersProxyModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid()) return 0;
    if (!sourceModel()) return 0;

    int count = sourceModel()->columnCount();

    return count;
}


int SubscribersProxyModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    if (!sourceModel())
        return 0;

    int count = 0;
    for (int root_row = 0; root_row < sourceModel()->rowCount(); root_row++)
        count += this->sourceRoot2Count(root_row);

    return count;
}


QVariant SubscribersProxyModel::headerData(int section,
                                           Qt::Orientation orientation,
                                           int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Vertical)
        return QVariant(section + 1);
    else
        return QSortFilterProxyModel::headerData(section, orientation, role);
}


QModelIndex SubscribersProxyModel::index(int row, int column,
                                         const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}


QModelIndex SubscribersProxyModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}
