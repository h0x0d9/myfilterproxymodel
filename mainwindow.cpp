#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "listsregistermodel.h"
#include <QtGui>
#include <QtCore>
#include "subscribersproxymodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    directModel = new ListsRegisterModel(this);

    proxyModel = new SubscribersProxyModel(this);
    proxyModel->setSourceModel(directModel);
//    proxyModel->setFilterKeyColumn(1);

    QTreeView *directModelView = new QTreeView(this);
    directModelView->setModel(directModel);
    QTableView *proxyModelView = new QTableView(this);
    proxyModelView->setModel(proxyModel);

    connect(directModelView,
            SIGNAL(clicked(QModelIndex)),
            SLOT(currentListChanged(QModelIndex)));

    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(directModelView);
    layout->addWidget(proxyModelView);
    QWidget *wgt = new QWidget(this);
    wgt->setLayout(layout);
    setCentralWidget(wgt);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::currentListChanged(const QModelIndex index)
{
    if (!index.isValid())
        return;

    if (index.column() != 0)
        return;

    if (directModel->data(index, Qt::DisplayRole).canConvert<QString>()) {
        QString currName = directModel->data(index, Qt::DisplayRole).toString();
        proxyModel->setFilterList(currName);
    }
}
