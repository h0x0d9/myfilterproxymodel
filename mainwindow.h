#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>

class SubscribersProxyModel;
class ListsRegisterModel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    SubscribersProxyModel *proxyModel;
    ListsRegisterModel *directModel;

private slots:
    void currentListChanged(const QModelIndex index);
};

#endif // MAINWINDOW_H
