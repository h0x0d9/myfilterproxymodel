#-------------------------------------------------
#
# Project created by QtCreator 2013-09-04T18:41:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyFilterProxyModel
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    treeitem.cpp \
    listsregistermodel.cpp \
    subscribersproxymodel.cpp

HEADERS  += mainwindow.h \
    treeitem.h \
    listsregistermodel.h \
    subscribersproxymodel.h

FORMS    += mainwindow.ui
