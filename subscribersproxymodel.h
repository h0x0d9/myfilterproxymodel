#ifndef SUBSCRIBERSPROXYMODEL_H
#define SUBSCRIBERSPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QtDebug>

class SubscribersProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit SubscribersProxyModel(QObject *parent = 0);

    void setSourceModel(QAbstractItemModel *model);
    void setFilterList(const QString listName);
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;
    int columnCount(const QModelIndex &parent) const;
    int rowCount(const QModelIndex &parent) const;
    QModelIndex mapFromSource(const QModelIndex &source) const;
    QModelIndex mapToSource(const QModelIndex &proxy) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const;
    QModelIndex index(int row, int column, const QModelIndex &parent
                      = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;

private:
    void setSourceRootsCounts() {
        int sourceRows = sourceModel()->rowCount();

        for (int row = 0; row < sourceRows; ++row)
            if (!sourceRootsCounts.contains(row)) {
                QModelIndex i = sourceModel()->index(row,0);
                sourceRootsCounts[row] = sourceModel()->rowCount(i);
            }
    }

    /**
      * @brief sourceRoot2Count считает число элементов второго уровня у row
      * @param row номер строки элемента верхнего уровня
      * @return число элементов второго уровня
      */
    int sourceRoot2Count(int row) const {

        if (!this->sourceModel())
            return 0;

        return sourceRootsCounts[row];
    }

    /**
      * @brief rowFromSource отображение root и row исходной модели
      * на номер строки в proxy
      * @param root номер строки корневого элемента исходной модели
      * @param row номер строки элемента второго уровня
      * @return номер строки в proxyModel
      */
    int rowFromSource(int root, int row) const{
        for (int r = 0; r < root; r++)
            row += sourceRoot2Count(r);
        return row;
    }

    /**
      * @brief rowToSource отображение row в proxy на root и row source
      * @param row номер строки в proxyModel
      * @return QPair<root, row> в исходной модели
      */
    QPair<int,int> rowToSource(int row) const {
        int root = 0;

        for (int r = 0; r < this->sourceModel()->rowCount(); r++) {
            root = r;
            int rows_in_root = sourceRoot2Count(r);
            if (row >= rows_in_root)
                row -= rows_in_root;
            else break;
        }

        return qMakePair(root, row);
    }

    QString listName;
    QHash<int, int> sourceRootsCounts;
};

#endif // SUBSCRIBERSPROXYMODEL_H
