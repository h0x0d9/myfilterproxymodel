#ifndef LISTSREGISTERMODEL_H
#define LISTSREGISTERMODEL_H

#include <QAbstractItemModel>
class TreeItem;

/**
 * @brief The ListsRegisterModel класс регистра списков получателей
 */
class ListsRegisterModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit ListsRegisterModel(QObject *parent = 0);
    ~ListsRegisterModel();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::CheckStateRole);

signals:
    
public slots:
    
private:
    enum Columns // нумерованный список колонок
    {
        RamificationColumn,
        ListNameColumn = RamificationColumn,
        ListSubscriberColumn,
        LastColumn
    };

    TreeItem *rootItem;

    /**
     * @brief parseListsFile разбираем структуру файла с описаниями списков
     * @param path путь к файлу с регистром списков
     * @return QMultiMap<имя_списка, адрес получателя>
     */
    QMultiMap<QString, QString> parseListsFile(QString path);

    /**
     * @brief setupModelData первоначальная настройка модели
     * @param lists multiMap с регистром списков
     * @param parent корневой элемент дерева
     */
    void setupModelData(QMultiMap<QString, QString> lists, TreeItem *parent);
    TreeItem *getItem(const QModelIndex &index) const;

};

#endif // LISTSREGISTERMODEL_H
